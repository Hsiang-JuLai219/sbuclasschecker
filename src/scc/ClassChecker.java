package scc;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.application.Platform;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Larry on 3/31/2017.
 */
public class ClassChecker implements Runnable {

    private static final Map<String, String> TERMS;
    static {
        Map<String, String> temp = new HashMap<>();
        temp.put("Fall 2018", "1188");
        TERMS = Collections.unmodifiableMap(temp);
    }
    private static final String AVAIL_TAG = "<SU_ENRL_AVAL>";
    private static final String AVAIL_CLOSE = "</SU_ENRL_AVAL>";
    private static final String WAIT_TAG = "<WAITLIST_POS>";
    private static final String WAIT_CLOSE = "</WAITLIST_POS>";

    private ObservableList<Course> courses;
    private boolean pause;
    private Controller controller;

    public ClassChecker(Controller controller)
    {
        this.controller = controller;
        this.courses = controller.data.getCourseList();
        pause = false;
    }

    @Override
    public void run()
    {
        while(true)
        {
            if(!pause)
                check();
            try {Thread.sleep(controller.data.getInterval());}
            catch (InterruptedException e) {e.printStackTrace();}
        }
    }

    private void check()
    {
        try
        {
            for (Course c : courses)
                updateCourse(c);
            Platform.runLater(() ->
            {
                controller.setUpdateTime("Last Update: " + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME));
                controller.table.refresh();
            });
        }
        catch (IOException e) {e.printStackTrace();}
    }

    private void updateCourse(Course c) throws IOException
    {
        URL url = new URL("http://classfind.stonybrook.edu/vufind/AJAX/JSON?method=getItemVUStatuses&itemid=" +
                c.getCode() + "&strm=" + TERMS.get(c.getTerm())); //1178 for fall 2017
        ObjectMapper mapper = new ObjectMapper();
        Map<String,String> map = mapper.readValue(url, Map.class);
        String data = map.get("data");

        String availString = data.substring(data.indexOf(AVAIL_TAG) + AVAIL_TAG.length(), data.indexOf(AVAIL_CLOSE));
        String waitString = data.substring(data.indexOf(WAIT_TAG) + AVAIL_TAG.length(), data.indexOf(WAIT_CLOSE));

        int available = Integer.parseInt(availString);
        int waitlist = Integer.parseInt(waitString);

        if(available != c.getAvailable() || waitlist != c.getWaitlist())
        {
            c.setAvailable(available);
            c.setWaitlist(waitlist);
            if(c.isAlarm())
                Platform.runLater(()->controller.enrollNow());
        }
    }

    public boolean isPause() {return pause;}

    public void setPause(boolean pause) {this.pause = pause;}
}
