package scc;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Larry on 3/31/2017.
 */
public class CourseData
{

    static final String[] availableTerms = new String[] {"Fall 2018"};

    private ObservableList<Course> courseList;
    private int interval;

    public CourseData()
    {
        courseList = FXCollections.observableArrayList();
        interval = 10000;
    }

    public void add(String name, int code, String term)
    {
        Course course = new Course(name, code, term);
        courseList.add(course);
        save();
    }

    public void remove(Course c)
    {
        courseList.remove(c);
        save();
    }

    public ObservableList<Course> getCourseList() {return courseList;}

    public int getInterval() {return interval;}

    public void setInterval(int interval) {this.interval = interval;}

    public void load()
    {
        String path = getClass().getResource("myList.json").getFile();
        File myList = new File(path);
        loadFromFile(myList);
    }

    public void loadFromFile(File from)
    {
        ObjectMapper mapper = new ObjectMapper();
        ArrayList<Course> courses = null;
        try {courses = mapper.readValue(from, CourseList.class);}
        catch (IOException e)
        {
            //e.printStackTrace();
            System.out.println("Failed to load the list. First time running?");
        }
        if(courses != null)
        {
            System.out.println("Course list has been loaded.");
            courseList.addAll(courses);
        }
    }

    public void save()
    {
        String path = getClass().getResource("myList.json").getFile();
        File myList = new File(path);
        saveAs(myList);
    }

    public void saveAs(File to)
    {
        ObjectMapper mapper = new ObjectMapper();
        try {mapper.writeValue(to, courseList);}
        catch (IOException e) {e.printStackTrace();}
    }
}

class CourseList extends ArrayList<Course>{}