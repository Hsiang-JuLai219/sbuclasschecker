package scc;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by Larry on 11/17/2016.
 */
public class Course implements Comparable<Course>{

    private String name;
    private int code;
    private String term;
    private int available;
    private int waitlist;
    private boolean alarm;

    public Course(){}

    public Course(String name, int code, String term)
    {
        this.name = name;
        this.code = code;
        this.term = term;
        available = -1;
        waitlist = -1;
        alarm = false;
    }

    public int getCode() {return code;}

    @JsonIgnore
    public String getCode_str() {return Integer.toString(code);}

    public void setCode(int code) {this.code = code;}

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public String getTerm() {return term;}

    public void setTerm(String term) {this.term = term;}

    public int getAvailable() {return available;}

    @JsonIgnore
    public String getAvailable_str() {return Integer.toString(available);}

    public void setAvailable(int available) {this.available = available;}

    public int getWaitlist() {return waitlist;}

    @JsonIgnore
    public String getWaitlist_str() {return Integer.toString(waitlist);}

    public void setWaitlist(int waitlist) {this.waitlist = waitlist;}

    public boolean isAlarm() {return alarm;}

    @JsonIgnore
    public String getAlarm_str() {return alarm ? "Yes" : "No";}

    public void setAlarm(boolean alarm) {this.alarm = alarm;}

    @Override
    public int compareTo(Course c) {return name.compareTo(c.name);}

    @Override
    public String toString(){return name + " in " + term;}
}
