package scc;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main_layout.fxml"));
        Parent root = loader.load();
        Controller controller = loader.<Controller>getController();
        controller.initWorkspace();
        primaryStage.setTitle("SBU Class Checker");
        primaryStage.setScene(new Scene(root, 700, 500));
        primaryStage.show();
        controller.startChecking();
    }


    public static void main(String[] args) {
        launch(args);
    }

}
