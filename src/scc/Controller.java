package scc;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

public class Controller {

    CourseData data;
    ClassChecker checker;

    @FXML
    VBox pane;

    /* ----- toolbar ------ */
    @FXML
    Button pause;
    @FXML
    Button alarm;
    @FXML
    Button saveAs;
    @FXML
    Button load;

    @FXML
    TextField freq_tf;
    @FXML
    Button freq_btn;
    /* -------------------- */

    /* ----- add course ----- */
    @FXML
    TextField name;
    @FXML
    TextField code;
    @FXML
    ChoiceBox<String> term;
    @FXML
    Button add;
    /* ---------------------- */

    /* ----- table ----- */
    @FXML
    TableView<Course> table;
    TableColumn<Course, String> classColumn;
    TableColumn<Course, String> termColumn;
    TableColumn<Course, String> codeColumn;
    TableColumn<Course, String> availColumn;
    TableColumn<Course, String> waitColumn;
    TableColumn<Course, String> alarmColumn;
    @FXML
    Label update;
    /* ----------------- */

    public Controller() {data = new CourseData();}

    public void startChecking()
    {
        data.load();
        table.refresh();
        checker = new ClassChecker(this);
        new Thread(checker).start();
    }

    public void pause()
    {
        checker.setPause(!checker.isPause());
        if(checker.isPause())
            pause.setText("Resume");
        else
            pause.setText("Pause");
    }

    public void addClass()
    {
        String courseName = name.getText();
        String courseTerm = term.getSelectionModel().getSelectedItem();
        int courseCode = Integer.parseInt(code.getText());
        data.add(courseName, courseCode, courseTerm);
        name.clear();
        code.clear();
    }

    public void setUpdateTime(String time) {update.setText(time);}

    public void setFreq()
    {
        int interval = 10000;
        String temp = freq_tf.getText();

        if(temp.matches("\\d+"))
        {
            interval = Integer.parseInt(temp);
            data.setInterval(interval);
        }
        else
        {
            freq_tf.setText(data.getInterval() + "");
            freq_tf.requestFocus();
            alertOnError("Please enter a positive integer.");
        }

    }

    public void handleSaveAs()
    {

    }

    public void handleLoad()
    {

    }

    public void toggleAlarm()
    {
        Course selected = table.getSelectionModel().getSelectedItem();
        if(selected != null)
        {
            selected.setAlarm(!selected.isAlarm());
            table.refresh();
        }
    }

    public void initWorkspace()
    {
        setupTable();
        term.setItems(FXCollections.observableArrayList(CourseData.availableTerms));
        freq_tf.setText(data.getInterval() + "");
        pane.setOnKeyPressed(event ->
        {
            switch (event.getCode())
            {
                case DELETE:
                    Course selected = table.getSelectionModel().getSelectedItem();
                    if(selected != null)
                        data.remove(selected);
                    table.refresh();
                    break;
                default:
            }

        });
    }

    public void enrollNow()
    {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("ENROLL NOW");
        alert.setHeaderText("Go To SOLAR Now!!!");
        alert.setContentText("The number of available seats or waitlest just changed.");
        alert.showAndWait();
    }

    private void alertOnError(String msg)
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Illegal Input Format.");
        alert.setContentText(msg);
        alert.showAndWait();
    }

    private void setupTable()
    {
        table.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        table.setItems(data.getCourseList());

        classColumn = (TableColumn) table.getColumns().get(0);
        termColumn = (TableColumn) table.getColumns().get(1);
        codeColumn = (TableColumn) table.getColumns().get(2);
        availColumn = (TableColumn) table.getColumns().get(3);
        waitColumn = (TableColumn) table.getColumns().get(4);
        alarmColumn = (TableColumn) table.getColumns().get(5);

        classColumn.setCellValueFactory(new PropertyValueFactory<Course, String>("name"));
        termColumn.setCellValueFactory(new PropertyValueFactory<Course, String>("term"));
        codeColumn.setCellValueFactory(new PropertyValueFactory<Course, String>("code_str"));
        availColumn.setCellValueFactory(new PropertyValueFactory<Course, String>("available_str"));
        waitColumn.setCellValueFactory(new PropertyValueFactory<Course, String>("waitlist_str"));
        alarmColumn.setCellValueFactory(new PropertyValueFactory<Course, String>("alarm_str"));
    }

}
